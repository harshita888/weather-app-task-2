import React, { Component } from 'react';
import axios from 'axios';
import Search from './Components/Search';
import Display from './Components/Display';

import './App.css';

class App extends Component {
  state={
    temp:null,
    description:null,
    selectdd:null,
    inputbox:''
    //name:null
  }
  arr=[];
handleChange = event => {
  this.setState({
    inputbox: event.target.value
  },this.call);
};

localStore=()=>{
  localStorage.setItem('key',JSON.stringify( this.arr))
}

resetInput=(event)=>{
 this.setState({
   selectdd: event.target.value,
   inputbox: ''
 })
}
baseURL='http://api.openweathermap.org/data/2.5/weather?';
apiID='&units=metric&APPID=95f3d73ba095ee111683e910575061b9';


call=()=>
{
  let list=this.state.inputbox;
  let link='';
    switch(this.state.selectdd){
      case 'q': 
      link=this.baseURL+this.state.selectdd+'='+list+this.apiID;
      break;
      case 'zip':
          link=this.baseURL+this.state.selectdd+'='+list+this.apiID;
          break;
      case 'lat lon':
          const [lat,lon]=this.state.selectdd.split(' ');
          const [inlat,inlon]=this.state.inputbox.split(',');
          link=this.baseURL+lat+'='+inlat+'&'+lon+'='+inlon+this.apiID;
          break;
      default:
    }

    console.log(link);
    axios.get(link)
    .then(response=>{
      this.setState({temp: response.data.main.temp,
      description: response.data.weather[0].description}
        );
        this.arr.push(response);
        this.localStore();     
    });

}
 
  
  render() {
    
    
    return (
      <div className="App">
        
          <form className="Search" >
            
            <input
              type="text"
              name="name"
              value={this.state.inputbox}
              onChange={this.handleChange}
              
            />
          </form>
   
         
        
        <select className="Dropdown"
        onChange={this.resetInput}>
          <option defaultValue>select</option>
  <option value="q">City Name</option>
  <option value="zip">Zip Code</option>
  <option value="lat lon">Lat/Long</option>
  </select>
  
  <Display temp={this.state.temp} description={this.state.description} />
  
      </div>
    );
  }
}

export default App;
