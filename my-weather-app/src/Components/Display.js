import React from 'react';

import './Display.css';

const display = (props)=>{


    const getElement=()=>{
        let getting = JSON.parse(localStorage.getItem('key'));
        let historystring = getting.slice(-3).reverse();
       return(<div className="Border">
           
        {historystring.map(post=>{
            return <span className="span"> {post.data.name}: {post.data.main.temp}°C</span> ;
            
          })}
          </div>
       )
       
       
    }
    return(
        <div>
<h2 className="Weather">Weather Info : {props.temp}°C({props.description})</h2>
<h4 className="Alignment">Past Searches:</h4>
            {getElement()}
            
        </div>
    
    );
}


export default display;