import React, { Component } from 'react';
import SearchField from 'react-search-field';
import './Search.css';



class Search extends Component {

    constructor(props){
      super(props);
      this.state = { name: '' };
    }
   
    handleChange = event => {
      this.setState({ name: event.target.value });
    };
   
    render() {
      return (
        <React.Fragment>
          <form className="Search">
            <label htmlFor="name"></label>
            <input
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.handleChange}
            />
          </form>
   
          {/* <h3> {this.state.name}</h3> */}
        </React.Fragment>
      );
    }
   }



   export default Search;